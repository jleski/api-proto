# Node.JS AWS Fargate API Demo

Small example Node.JS service running in Docker container on AWS Fargate.

Comes preconfigured with AWS Fargate environment called 'demo'. See **mu.yml** for more information.

# Deploy

Setup environment:
```sh
mu env demo up
```

Deploy service:
```sh
mu svc deploy demo
```

That's it!

## Author
Jaakko Leskinen <<jaakko.leskinen@gmail.com>>